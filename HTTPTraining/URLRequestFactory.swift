//
//  URLRequestFactory.swift
//  HTTPTraining
//
//  Created by Dalton Claybrook on 6/15/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

import Foundation

class URLRequestFactory: NSObject {
    
    func urlRequestFromNetworkRequest(request: NetworkRequest) throws -> NSURLRequest {
        
        let urlRequest = NSMutableURLRequest(URL: request.url)
        urlRequest.HTTPMethod = self.httpMethodFromRequestType(request.requestType)
        if let serializer = request.requestSerializer, data: AnyObject = request.bodyData {
            
            let bodyData = try serializer.requestBodyWithData(data)
            urlRequest.HTTPBody = bodyData
            urlRequest.setValue(serializer.contentType, forHTTPHeaderField: "Content-Type")
        }
        
        return urlRequest.copy() as! NSURLRequest
    }
    
    // Private
    
    private func httpMethodFromRequestType(type: RequestType) -> String {
        switch type {
            case .GET:
                return "GET"
            case .POST:
                return "POST"
            case .PUT:
                return "PUT"
            case .DELETE:
                return "DELETE"
        }
    }
}
