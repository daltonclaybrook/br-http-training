//
//  NetworkRequest.swift
//  HTTPTraining
//
//  Created by Dalton Claybrook on 6/12/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

import Foundation

enum RequestType {
    case GET, POST, PUT, DELETE
}

struct NetworkRequest {
    
    let requestType: RequestType
    let url: NSURL
    var bodyData: AnyObject?
    
    var requestSerializer: RequestSerialization?
    var responseSerializer: ResponseSerialization?
    
    init(requestType: RequestType, url: NSURL) {
        self.requestType = requestType
        self.url = url
    }
    
    static func GET(url: NSURL) -> NetworkRequest {
        return self.init(requestType: .GET, url: url)
    }
    
    static func POST(url: NSURL, data: AnyObject) -> NetworkRequest {
        var request = self.init(requestType: .POST, url: url)
        request.bodyData = data
        return request
    }
    
    static func PUT(url: NSURL, data: AnyObject) -> NetworkRequest {
        var request = self.init(requestType: .PUT, url: url)
        request.bodyData = data
        return request
    }
    
    static func DELETE(url: NSURL) -> NetworkRequest {
        return self.init(requestType: .DELETE, url: url)
    }
}
