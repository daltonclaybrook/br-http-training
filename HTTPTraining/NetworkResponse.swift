//
//  NetworkResponse.swift
//  HTTPTraining
//
//  Created by Dalton Claybrook on 6/15/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

import Foundation

enum ResponseCodeType {
    case Unknown
    case Success
    case Redirection
    case ClientError
    case ServerError
}

enum ResponseCodeSubtype: Int {
    case Unknown = -1
    case OK = 200
    case Created = 201
    case BadRequest = 400
    case Unauthorized = 401
    case Forbidden = 403
    case NotFound = 404
    case InternalServerError = 500
}

class NetworkResponse: NSObject {
    
    let codeType: ResponseCodeType
    let codeSubtype: ResponseCodeSubtype
    let error: NSError?
    let result: AnyObject?
    
    override var description: String {
        return "\(self.codeType) : \(self.codeSubtype)\nerror: \(self.error)\n\n\(self.result)"
    }
    
    init(codeType: ResponseCodeType, codeSubtype: ResponseCodeSubtype, error: NSError?, result: AnyObject?) {
        self.codeType = codeType
        self.codeSubtype = codeSubtype
        self.error = error
        self.result = result
    }
}
