//
//  ResponseSerialization.swift
//  HTTPTraining
//
//  Created by Dalton Claybrook on 6/15/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

import Foundation

protocol ResponseSerialization {
    
    func responseBodyWithData(data: NSData?) throws -> AnyObject?
}
