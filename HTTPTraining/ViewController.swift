//
//  ViewController.swift
//  HTTPTraining
//
//  Created by Dalton Claybrook on 6/12/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var networkManager = NetworkManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = NSURL(string: "http://jsonplaceholder.typicode.com/posts") {
            
            let postData = [ "title" : "Test Title",
                                "body" : "Test Body" ]
            
            var request = NetworkRequest.POST(url, data: postData)
//            var request = NetworkRequest.GET(url)
            
            let serializer = JSONSerializer()
            request.requestSerializer = serializer
            request.responseSerializer = serializer
            
            try! self.networkManager.sendRequest(request) { (response) in
                
                if let result: AnyObject = response.result {
                    print("result: \(result)")
                } else if let error = response.error {
                    print("error: \(error)")
                } else {
                    print("something went wrong")
                }
            }
        }
    }
}

