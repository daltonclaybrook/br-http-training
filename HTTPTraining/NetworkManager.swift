//
//  NetworkManager.swift
//  HTTPTraining
//
//  Created by Dalton Claybrook on 6/12/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

import Foundation

typealias NetworkManagerCompletion = (response: NetworkResponse) -> Void;

enum NetworkManagerError: ErrorType {
    case SerializerMismatch
}

class NetworkManager: NSObject {
    
    // Private Properties
    private let urlSession = NSURLSession.sharedSession()
    private let requestFactory = URLRequestFactory()
    private let responseFactory = NetworkResponseFactory()
    
    func sendRequest(request: NetworkRequest, block: NetworkManagerCompletion) throws {
        
        do {
            let urlRequest = try self.requestFactory.urlRequestFromNetworkRequest(request)
            self.urlSession.dataTaskWithRequest(urlRequest, completionHandler: { [unowned self] (data, response, error) -> Void in
                
                let response = self.responseFactory.responseFromData(data, serializer: request.responseSerializer, urlResponse: response, error: error)
                block(response: response)
            }).resume()
            
        } catch {
            throw NetworkManagerError.SerializerMismatch
        }
    }
}
