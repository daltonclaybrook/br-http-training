//
//  RequestSerialization.swift
//  HTTPTraining
//
//  Created by Dalton Claybrook on 6/12/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

import Foundation

protocol RequestSerialization {
    
    var contentType: String { get }
    
    func requestBodyWithData(data: AnyObject?) throws -> NSData?
}
