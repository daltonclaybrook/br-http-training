//
//  JSONSerializer.swift
//  HTTPTraining
//
//  Created by Dalton Claybrook on 6/12/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

import Foundation

class JSONSerializer: NSObject, RequestSerialization, ResponseSerialization {
    
    // RequestSerialization
    var contentType: String = "application/json"
    
    func requestBodyWithData(data: AnyObject?) throws -> NSData? {
        
        var outData: NSData? = nil
        if let data = data {
            outData = try NSJSONSerialization.dataWithJSONObject(data, options: .PrettyPrinted)
        }
        
        return outData
    }
    
    // ResponseSerialization
    func responseBodyWithData(data: NSData?) throws -> AnyObject? {
        
        var outData: AnyObject? = nil
        if let data = data where data.length > 0 {
            outData = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers)
        }
        
        return outData
    }
}
