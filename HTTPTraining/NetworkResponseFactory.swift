//
//  NetworkResponseFactory.swift
//  HTTPTraining
//
//  Created by Dalton Claybrook on 6/15/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

import Foundation

let NetworkManagerErrorDomain = "NetworkManagerErrorDomain"
let NetworkManagerThrownErrorCode = -1

class NetworkResponseFactory: NSObject {
    
    private var errorMappings = [Int : [String:AnyObject]]()
    
    // Public
    func responseFromData(data: NSData?, serializer: ResponseSerialization?, urlResponse: NSURLResponse?, error: NSError?) -> NetworkResponse {
        
        var type = ResponseCodeType.Unknown
        var subtype = ResponseCodeSubtype.Unknown
        var outData: AnyObject? = data
        var outError = error
        
        if let urlResponse = urlResponse as? NSHTTPURLResponse where (error == nil) {
            
            type = self.responseCodeTypeFromCode(urlResponse.statusCode)
            subtype = self.responseCodeSubtypeFromCode(urlResponse.statusCode)
            if type == .Success {
                if let serializer = serializer  {
                    do {
                        outData = try serializer.responseBodyWithData(data)
                    } catch let thrown {
                        outError = self.errorForThrownError(thrown)
                    }
                }
            } else {
                outError = self.errorForCode(urlResponse.statusCode)
            }
        }
        
        return NetworkResponse(codeType: type, codeSubtype: subtype, error: outError, result: outData)
    }
    
    func registerErrorUserInfo(userInfo: [String:AnyObject], forStatusCode code: Int) {
        self.errorMappings[code] = userInfo
    }
    
    // Private
    private func responseCodeTypeFromCode(code: Int) -> ResponseCodeType {
        let codeString = String(code)
        switch codeString[codeString.startIndex] {
        case "2":
            return .Success
        case "3":
            return .Redirection
        case "4":
            return .ClientError
        case "5":
            return .ServerError
        default:
            return .Unknown
        }
    }
    
    private func responseCodeSubtypeFromCode(code: Int) -> ResponseCodeSubtype {
        switch code {
        case 200:
            return .OK
        case 201:
            return .Created
        case 400:
            return .BadRequest
        case 401:
            return .Unauthorized
        case 403:
            return .Forbidden
        case 404:
            return .NotFound
        case 500:
            return .InternalServerError
        default:
            return .Unknown
        }
    }
    
    private func errorForCode(code: Int) -> NSError {
        var info = self.unknownUserInfo()
        if let registeredInfo = self.errorMappings[code] {
            info = registeredInfo
        }
        return NSError(domain: NetworkManagerErrorDomain, code: code, userInfo: info)
    }
    
    private func errorForThrownError(error: ErrorType) -> NSError {
        return NSError(domain: NetworkManagerErrorDomain, code: NetworkManagerThrownErrorCode, userInfo: nil)
    }
    
    private func unknownUserInfo() -> [String:AnyObject] {
        return [NSLocalizedDescriptionKey : "Unknown error"]
    }
}
